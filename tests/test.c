#define STAGE_TEST 1
#include "main.c"

typedef struct {
	bool error;
	const char *test;
	const char *message;
	const char *function;
	const char *file;
	u64 line;
} test_result;

typedef struct {
	u64 tests_run;
	u64 tests_succeed;
} test_data;

void print_error(test_result res) {
	if (res.error) {
		const char *file = res.file;
		const char *function = res.function;
		if (!file) file = "(unknown)";
		if (!function) function = "(unknown)";
		fprintf(stdout, "%s(%lu) %s", file, res.line, function);
		if (res.test) {
			fprintf(stdout, ": %s", res.test);
		}
		if (res.message) {
			fprintf(stdout, ": %s", res.message);
		}
		fprintf(stdout, "\n");
	}
}

#define test_assert_msg(expr, msg) do {			\
		if (!(expr)) {							\
			return (test_result){				\
				.error=true,					\
					.test=#expr,				\
					.message=msg,				\
					.function=__func__,			\
					.file=__FILE__,				\
					.line=__LINE__				\
					}; } } while(0);

#define test_assert(expr) test_assert_msg(expr, 0)

#define ok() (test_result){						\
		.error=false,							\
			.test=0,							\
			.message=0,							\
			.function=__func__,					\
			.line=__LINE__						\
			}

#define run_test(test) do {						\
		test_result res = test();				\
		if (res.error){print_error(res);}		\
		else{++data.tests_succeed;} \
		++data.tests_run; } while(0);

#define run_testset(testset) do {					\
		test_data res = testset();					\
		data.tests_run += res.tests_run;			\
		data.tests_succeed += res.tests_succeed;	\
	} while(0);

test_result test_true() {
	test_assert(true);

	return ok();
}

#include "test_func.c"
#include "test_channel.c"

int
main() {
	test_data data = {};

	run_test(test_true);
	run_testset(test_func);
	run_testset(test_channel);

	u64 tests_failed = data.tests_run-data.tests_succeed;
	float success_percent = 100;
	if (data.tests_run > 0) {
		success_percent = ((float)data.tests_succeed / (float)data.tests_run) * 100.0f;
	}
	fprintf(stdout, "=========================\n");
	fprintf(stdout, "Tests finished\n");
	fprintf(stdout, "Success: %lu/%lu (%.1f%%)\n",
			data.tests_succeed,
			data.tests_run,
			success_percent);
	if (tests_failed > 0) {
		fprintf(stdout, "%lu TESTS FAILED\n", tests_failed);
	}

	return tests_failed > 0;
}
