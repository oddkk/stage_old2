test_result
test_channel_external() {
	u8 transient_data[1000] = {};
	struct arena transient = {
		.data = transient_data,
		.capacity = sizeof(transient_data),
	};

	struct channel_range range = {
		.channel_begin = 0,
		.channel_end = 2,
	};
	range.channels = calloc(range.channel_end-range.channel_begin, sizeof(struct channel));

	u8 data[] = {
		0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // PUSH_CNL 1
	};
	u64 deps[] = { 1 };
	
	struct func func = {
		.instr = data,
		.instr_len = sizeof(data),
		.dependencies = (u64*)deps,
		.dependencies_len = sizeof(deps) / sizeof(u64),
	};

	u8 error = func_validate(&func);
	if (error) {
		assert(false);
	}

	struct channel *cnl1 = channel_get(&range, 1);

	channel_set_func(&transient, &range, 0, &func);

	cnl1->flags |= CHANNEL_EXTERNAL_INPUT;
	cnl1->value = 2;

	transient.allocated = 0;
	u64 val = channel_get_value(&transient, &range, 0);

	test_assert(val == 2);
	
	return ok();
}

test_result
test_channel_func() {
	u8 transient_data[1000];
	struct arena transient = {
		.data = transient_data,
		.capacity = sizeof(transient_data),
	};

	struct channel_range range = {
		.channel_begin = 0,
		.channel_end = 2,
	};
	range.channels = calloc(range.channel_end-range.channel_begin, sizeof(struct channel));

	u8 data[] = {
		0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // PUSH_CNL 1
	};
	u64 deps[] = { 1 };
	
	struct func func = {
		.instr = data,
		.instr_len = sizeof(data),
		.dependencies = (u64*)deps,
		.dependencies_len = sizeof(deps) / sizeof(u64),
	};

	u8 error = func_validate(&func);
	if (error) {
		assert(false);
	}

	u8 data2[] = {
		0x01, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // PUSH_LIT 7
	};
	u64 deps2[] = { 1 };
	
	struct func func2 = {
		.instr = data2,
		.instr_len = sizeof(data2),
		.dependencies = (u64*)deps2,
		.dependencies_len = 0,
	};

	error = func_validate(&func2);
	if (error) {
		assert(false);
	}

	channel_set_func(&transient, &range, 0, &func);
	transient.allocated = 0;
	channel_set_func(&transient, &range, 1, &func2);

	transient.allocated = 0;
	u64 val = channel_get_value(&transient, &range, 0);

	test_assert(val == 7);
	
	return ok();
}

test_data test_channel() {
	test_data data = {};

	run_test(test_channel_external);
	run_test(test_channel_func);

	return data;
}
