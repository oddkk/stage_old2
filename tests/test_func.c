test_result
test_func_add() {
	u8 transient_data[1000];
	struct arena transient = {
		.data = transient_data,
		.capacity = sizeof(transient_data),
	};

	u8 instr[] = {
		0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x05,
	};
	u64 deps[] = {0};

	u64 params[] = {0};

	struct func func = {
		.instr = instr,
		.instr_len = sizeof(instr),
		.dependencies = deps,
		.dependencies_len = 0,
		.stack_requirement = 4
	};

	func.flags |= FUNC_VALIDATED;

	u64 res = func_eval(&transient, &func, params, 0);

	test_assert(res == 3);

	return ok();
}

test_result
test_func_verify_valid() {
	u8 instr[] = {
		0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x05,
	};
	u64 deps[] = {0};

	struct func func = {
		.instr = instr,
		.instr_len = sizeof(instr),
		.dependencies = deps,
		.dependencies_len = 0
	};

	u8 error = func_validate(&func);

	test_assert(!error);

	return ok();
}

test_result
test_func_verify_unexpected_eof() {
	u8 instr[] = {
		0x01, 0x01,
	};
	u64 deps[] = {0};

	struct func func = {
		.instr = instr,
		.instr_len = sizeof(instr),
		.dependencies = deps,
		.dependencies_len = 0
	};

	u8 error = func_validate(&func);

	test_assert(error & FUNC_VALIDATE_UNEXPECTED_EOF);

	return ok();
}

test_result
test_func_verify_invalid_missing_stack() {
	u8 instr[] = {
		0x05,
	};
	u64 deps[] = {0};

	struct func func = {
		.instr = instr,
		.instr_len = sizeof(instr),
		.dependencies = deps,
		.dependencies_len = 0
	};

	u8 error = func_validate(&func);

	test_assert(error & FUNC_VALIDATE_POP_PAST_BASE);

	return ok();
}

test_result
test_func_verify_invalid_return_too_many() {
	u8 instr[] = {
		0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	};
	u64 deps[] = {0};

	struct func func = {
		.instr = instr,
		.instr_len = sizeof(instr),
		.dependencies = deps,
		.dependencies_len = 0
	};

	u8 error = func_validate(&func);

	test_assert(error & FUNC_VALIDATE_STACK_NOT_ONE_LEFT);

	return ok();
}

test_data test_func() {
	test_data data = {};

	run_test(test_func_add);
	run_test(test_func_verify_valid);
	run_test(test_func_verify_unexpected_eof);
	run_test(test_func_verify_invalid_missing_stack);
	run_test(test_func_verify_invalid_return_too_many);

	return data;
}
