#!/bin/bash

CC=clang
VERSION=0.1

$CC -Wall -Wextra -pedantic -Wno-gnu-empty-initializer -Wno-unused-function -DVERSION=\"${VERSION}\" -D_DEFAULT_SOURCE -D_POSIX_C_SOURCE=200809L -g -std=c11 -licuio -lrt -lpthread -luv -o stage -Isrc/ src/utf8proc/utf8proc.c src/main.c
