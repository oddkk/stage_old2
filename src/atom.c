struct atom {
	u64 hash;
	struct string name;
	struct atom *next_in_bucket;
};

#define atom_page_capacity (10)

struct atom_page {
	struct atom_page *next;
	u64 num_used;
	struct atom atoms[atom_page_capacity];
};

struct atom_table {
	struct arena *string_arena;
	struct atom_page *first_page;
	struct atom_page *last_page;

	u64 count;

	u64 num_buckets;
	struct atom **buckets;
};

static void
atom_table_insert(struct atom_table *table, struct atom *new_atom) {
	struct atom **current;

	current = &table->buckets[new_atom->hash % table->num_buckets];

	while (*current && (*current)->hash <= new_atom->hash) {
		if ((*current)->hash == new_atom->hash) {
			if (string_equals_icase(new_atom->name, (*current)->name)) {
				break;
			}
		}
		current = &(*current)->next_in_bucket;
	}

	new_atom->next_in_bucket = *current;
	*current = new_atom;
}

static void
atom_table_rehash(struct atom_table *table, u64 new_num_buckets) {
	if (new_num_buckets == table->num_buckets) {
		return;
	}

	assert(new_num_buckets > table->num_buckets);

	struct atom **tmp_buckets = table->buckets;
	u64 tmp_num_buckets = table->num_buckets;

	table->num_buckets = new_num_buckets;
	table->buckets = calloc(table->num_buckets, sizeof(struct atom *));

	if (tmp_num_buckets == 0 && !tmp_buckets) {
		return;
	}

	if (!table->buckets) {
		table->num_buckets = tmp_num_buckets;
		table->buckets = tmp_buckets;
		return;
	}

	for (u64 i = 0; i < tmp_num_buckets; ++i) {
		struct atom *current = tmp_buckets[i];
		while (current) {
			struct atom *next = current->next_in_bucket;
			atom_table_insert(table, current);
			current = next;
		}
	}

	if (tmp_buckets) {
		free(tmp_buckets);
	}
}

static struct atom *
atom_try_find(struct atom_table *table, struct string name) {
	assert(table);
	assert(name.text);
	u64 name_hash = string_hash_u8_icase(name);
	struct atom **current;
	
	current = &table->buckets[name_hash % table->num_buckets];

	while (*current && (*current)->hash <= name_hash) {
		if ((*current)->hash == name_hash) {
			if (string_equals_icase(name, (*current)->name)) {
				return *current;
			}
		}
		current = &(*current)->next_in_bucket;
	}

	return 0;
}

static struct atom *
atom_create(struct atom_table *table, struct string name) {
	assert(table);
	assert(name.text);
	u64 name_hash = string_hash_u8_icase(name);
	struct atom **current;
	
	current = &table->buckets[name_hash % table->num_buckets];

	while (*current && (*current)->hash <= name_hash) {
		if ((*current)->hash == name_hash) {
			if (string_equals_icase(name, (*current)->name)) {
				return *current;
			}
		}
		current = &(*current)->next_in_bucket;
	}
	
	// NOTE: The atom did not already exist. Append atom to bucket.
	
	// NOTE: Get a new atom from the last atom page. If the last page
	// is full, allocate a new one.
	struct atom *new_atom;
	if (!table->last_page || table->last_page->num_used >= atom_page_capacity) {
		struct atom_page *new_page = arena_alloc(table->string_arena, 1, sizeof(struct atom_page));

		if (!new_page) {
			fprintf(stderr, "Could not allocate new atom page.\n");
		}

		if (table->last_page) {
			table->last_page->next = new_page;
		} else {
			assert(!table->first_page);
			table->first_page = new_page;
		}
		table->last_page = new_page;
	}
	new_atom = &table->last_page->atoms[table->last_page->num_used++];

	new_atom->hash = name_hash;
	arena_duplicate_string(table->string_arena, &new_atom->name, name);

	new_atom->next_in_bucket = *current;
	*current = new_atom;

	++table->count;

	return new_atom;
}
