struct conf_entry *
conf_find_next_sibling(struct conf_entry **it, struct atom *name) {
	assert(it);
	if (!*it) {
		return 0;
	}
	while ((*it)->next_sibling != 0) {
		struct conf_entry *e = *it;
		*it = (*it)->next_sibling;
		if (e->name == name) {
			return e;
		}
	}
	return 0;
}

struct conf_entry *
conf_find_child(struct conf_entry *parent, struct atom *name) {
	if (!parent) {
		return 0;
	}
	struct conf_entry *child, *it = parent->first_child;
	if ((child = conf_find_next_sibling(&it, name))) {
		return child;
	}
	return 0;
}

u64
conf_entry_as_u64(struct conf_entry *entry) {
	if (!entry || entry->type != CONF_ENTRY_VALUE || !entry->value.text) {
		return 0;
	}
	// TODO: Use a better to number funciton?
	u64 res = strtol((char *)entry->value.text, NULL, 0);

	return res;
}

struct string
conf_entry_as_string(struct conf_entry *entry, struct arena *mem) {
	struct string res = {};
	if (entry && entry->type == CONF_ENTRY_VALUE && entry->value.text) {
		arena_duplicate_string(mem, &res, entry->value);
	}
	return res;
}

struct atom *
conf_entry_as_atom(struct conf_entry *entry, struct atom_table *table) {
	struct atom *res = 0;
	if (entry && entry->type == CONF_ENTRY_VALUE && entry->value.text) {
		res = atom_create(table, entry->value);
	}
	return res;
}
