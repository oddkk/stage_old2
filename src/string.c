typedef s32 rune;

struct string {
	u8 *text;
	u64 len;
};

struct string_position {
	u64 line;
	u64 col;
};

#define LIT(x) ((int)(x).len),((char*)(x).text)
#define STR_FROM_CSTR(STR) (struct string){.text=(u8*)STR,.len=sizeof(STR)-1}

static rune
rune_normalize(rune c) {
	if (c >= 'A' && c <= 'Z') {
		c |= 0x20;
	}
	return c;
}

static inline bool rune_is_alpha(rune c) {
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

static inline bool rune_is_num(rune c) {
	return (c >= '0' && c <= '9');
}

static inline bool rune_is_alphanum(rune c) {
	return rune_is_alpha(c) || rune_is_num(c);
}

static inline bool rune_is_whitespace(rune c) {
	return c == ' ' || c == '\t' || c == '\n';
}

bool string_equals_icase(struct string lhs, struct string rhs) {
	if (lhs.len != rhs.len) {
		return false;
	}
	u64 bytes_read = 0;
	while (bytes_read < lhs.len) {
		rune rune_lhs;
		s64 bytes_lhs
			= utf8proc_iterate((utf8proc_uint8_t*)(lhs.text + bytes_read),
							   lhs.len - bytes_read,
							   (utf8proc_int32_t*)&rune_lhs);
		rune rune_rhs;
		s64 bytes_rhs
			= utf8proc_iterate((utf8proc_uint8_t*)(rhs.text + bytes_read),
							   rhs.len - bytes_read,
							   (utf8proc_int32_t*)&rune_rhs);

		// TODO: Use a better approach for case folding.
		if (bytes_lhs < 0 || bytes_rhs != bytes_lhs ||
			rune_lhs == -1 || rune_normalize(rune_lhs) != rune_normalize(rune_rhs)) {
			return false;
		}
		bytes_read += bytes_lhs;
	}
	return true;
}

static inline bool
string_read_rune(struct string str, u64 *bytes_read, rune *out) {
	if (*bytes_read >= str.len) {
		return false;
	}
	s64 bytes
		= utf8proc_iterate((utf8proc_uint8_t*)(str.text + *bytes_read),
						   str.len - *bytes_read,
						   (utf8proc_int32_t*)out);

	if (bytes <= 0) {
		return false;
	}
	*bytes_read += bytes;
	return true;
}
static void
arena_duplicate_string(struct arena *arena, struct string *dest, struct string src) {
	dest->len = src.len;
	dest->text = arena_alloc(arena, dest->len, 1);
	copy_memory(dest->text, src.text, dest->len);
}

// NOTE: djb2 hash function. (http://www.cse.yorku.ca/~oz/hash.html)
static u64
string_hash_u8_icase(struct string str) {
	u64 hash = 5381;
	u64 bytes_read = 0;
	rune c;

	// TODO: Use a better approach for case folding.
	while (bytes_read < str.len) {
		s64 bytes
			= utf8proc_iterate((utf8proc_uint8_t*)(str.text + bytes_read),
							   str.len - bytes_read,
							   (utf8proc_int32_t*)&c);
		if (bytes < 0) {
			fprintf(stderr, "utf8 error: %s\n", utf8proc_errmsg(bytes_read));
			break;
		} else if (c == -1) {
			fprintf(stderr, "utf8 error: Valid codepoint could not be read.\n");
			break;
		}
		bytes_read += (u64)bytes;

		hash = ((hash << 5) + hash) + rune_normalize(c);
	}
	return hash;
}
