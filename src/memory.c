#define KILOBYTES(x) (((u64)x)*1000)
#define MEGABYTES(x) (KILOBYTES(x)*1000)
#define GIGABYTES(x) (MEAGBYTES(x)*1000)

#define KIBIBYTES(x) (((u64)x)*1024)
#define MEBIBYTES(x) (KILOBYTES(x)*1024)
#define GIBIBYTES(x) (MEAGBYTES(x)*1024)

#define U8_MAX (0xff)
#define U16_MAX (0xffff)
#define U32_MAX (0xffffffff)
#define U64_MAX (0xffffffffffffffff)

void zero_memory(void *ptr, u64 size) {
	while (size-- > 0) {
		*(((u8*)ptr) + size) = 0;
	}
}

void copy_memory(void *dest, void *src, u64 size) {
	while (size--) {
		*(((u8*)dest) + size) = *(((u8*)src) + size);
	}
}

#define zero_struct(x) zero_memory((void*)&(x), sizeof(x))

struct arena {
	u8 *data;
	u64 capacity;
	u64 allocated;
	u64 num_allocs;
};

void*
arena_alloc(struct arena *arena, u64 nmemb, u64 size) {
	void *result = &arena->data[arena->allocated];
	u64 num_bytes;
	bool error;

	// TODO: Portability
	error = __builtin_umull_overflow(nmemb, size, &num_bytes);
	assert(!error);
	error = __builtin_uaddl_overflow(arena->allocated, num_bytes, &arena->allocated);
	assert(!error);
	assert(arena->allocated < arena->capacity);

	++arena->num_allocs;

	zero_memory(result, num_bytes);
	return result;
}

u64
arena_push_state(struct arena *arena) {
	return arena->allocated;
}

void
arena_pop_state(struct arena *arena, u64 state) {
	assert(state <= arena->allocated);
	arena->allocated = state;
}

static inline u8
read_u8(u8 **p, u8 *end) {
	if (*p + sizeof(u8) > end) {
		*p = end;
		return 0;
	}
	return *((*p)++);
}

static inline u64
read_u64(u8 **p, u8 *end) {
	if (*p + sizeof(u64) > end) {
		*p = end;
		return 0;
	}
	u64 **q = (u64**)p;
	return *((*q)++);
}

static inline void
write_u8(u8 **p, void *end, u8 v) {
	u8 **q = (u8**)p;
	if (&(*q)[1] > (u8*)end) {
		*p = end;
		return;
	}
	*((*q)++) = v;
}

static inline void
write_u64(u8 **p, void *end, u64 v) {
	u64 **q = (u64**)p;
	if (&(*q)[1] > (u64*)end) {
		*p = end;
		return;
	}
	*((*q)++) = v;
}
