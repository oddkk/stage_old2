enum ease_type {
	EASE_IN  = (1<<0),
	EASE_OUT = (1<<1);
};

struct change {
	u64 begin_tick;
	u64 duration;
	channel_value end_value;
	u8 ease_type;
};
