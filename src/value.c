/* enum value_type_flags { */
/* 	VALUE_FLAG_SET_MASK = 0x1, */
/* 	VALUE_FLAG_UNSET = 0x0, */
/* 	VALUE_FLAG_SET = 0x1, */

/* 	VALUE_FLAG_BASE_MASK = 0x2, */
/* 	VALUE_FLAG_INT = 0x0, */
/* 	VALUE_FLAG_FLOAT = 0x2, */
/* }; */

enum value_type {
	VALUE_UNSET = 0,

	// An integral 32-bit number
	VALUE_NUMBER = 1,
	// A rational number in the range [0, 1]
	VALUE_UNIT = 2,
	// A rational number in the range [-1, 1]
	VALUE_NORMAL = 3,
};

struct value_t {
	s32 val;
	u32 type;
};

typedef struct value_t value;

static inline value
value_number_from_s32(s64 val) {
	value v;
	v.type = VALUE_NUMBER;
	v.val = val;
	return v;
}

static inline value
value_unit_from_s32(s64 val) {
	value v;
	v.type = VALUE_UNIT;
	v.val = val;
	if (v.val < 0) {
		v.val = 0;
	}
	return v;
}

static inline value
value_normal_from_s32(s64 val) {
	value v;
	v.type = VALUE_NORMAL;
	v.val = val;
	return v;
}

static inline value
value_number_from_su(u8 val) {
	value v;
	v.type = VALUE_NUMBER;
	v.val = (s32)val;
	return v;
}

static inline value
value_unit_from_u8(u8 val) {
	value v;
	v.type = VALUE_UNIT;
	s64 new_val = ((u64)val * U32_MAX) / U8_MAX;
	if (new_val > S32
	v.val = ;
	return v;
}

static inline value
value_from_unset() {
	value v;
	v.type = VALUE_UNSET;
	return v;
}

static inline bool
value_is_set(value value) {
	return value.type == VALUE_SET;
}

/* static inline bool */
/* value_is_set(value value) { */
/* 	return (value.type & VALUE_FLAG_SET_MASK) == VALUE_FLAG_SET; */
/* } */

static inline u64
value_get_u64(value v) {
	if (!value_is_set(v)) {
		return 0;
	}
	return v.v_u64;
}

/*
static inline bool
value_is_int(value value) {
	return value_is_set(value) && (value.type & VALUE_FLAG_BASE_MASK) == VALUE_FLAG_INT;
}

static inline bool
value_is_float(value value) {
	return value_is_set(value) && (value.type & VALUE_FLAG_BASE_MASK) == VALUE_FLAG_FLOAT;
}

static inline u64
value_get_u64(value v) {
	if (!value_is_set(v)) {
		return 0;
	}
	if (value_is_float(v)) {
		return v.v_number;
	}
	return v.v_u64;
}

static inline f64
value_get_number(value v) {
	if (!value_is_set(v)) {
		return 0.0;
	}
	if (value_is_int(v)) {
		return (f64)v.v_u64;
	}
	return v.v_number;
}

static inline u64
value_determine_type_for_binary_op(value lhs, value rhs) {
	if (value_is_float(lhs) || value_is_float(rhs)) {
	}
}

static inline value
value_add(value lhs, value rhs) {
	if (value_is_float(lhs) || value_is_float(rhs)) {
	}
	return value_from_u64(value_get(lhs) + value_get(rhs));
}
*/

static inline value
value_add(value lhs, value rhs) {
	return value_from_u64(value_get_u64(lhs) + value_get_u64(rhs));
}

static inline value
value_subtract(value lhs, value rhs) {
	return value_from_u64(value_get_u64(lhs) - value_get_u64(rhs));
}

static inline value
value_multiply(value lhs, value rhs) {
	return value_from_u64(value_get_u64(lhs) * value_get_u64(rhs));
}

static inline value
value_divide(value lhs, value rhs) {
	return value_from_u64(value_get_u64(lhs) / value_get_u64(rhs));
}

static inline value
value_mod(value lhs, value rhs) {
	return value_from_u64(value_get_u64(lhs) % value_get_u64(rhs));
}

static inline value
value_ifelse(value condition, value case_true, value case_false) {
	return value_from_u64(value_get_u64(condition) != 0 ? value_get_u64(case_true) : value_get_u64(case_false));
}
