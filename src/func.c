enum func_flags {
	FUNC_VALIDATED = (1<<0),
};

struct func {
	u8 *instr;
	u64 instr_len;
	u64 *dependencies;
	u64 dependencies_len;
	u64 stack_requirement;
	u8 flags;
};

enum func_opcode {
	OP_NOP=0x00,
	OP_PUSH_LIT=0x01,
	OP_PUSH_CNL=0x02,
	OP_PUSH_TICK=0x03,
	OP_PUSH_UNSET=0x04,
	OP_ADD=0x05,
	OP_SUB=0x06,
	OP_MUL=0x07,
	OP_DIV=0x08,
	OP_MOD=0x09,
	OP_IFELSE=0x0a,
};

struct {
	enum func_opcode op;
	u64 num_args;
	u64 num_push;
	u64 num_pop;
} func_op_data[] = {
	{ OP_NOP,        0, 0, 0 },
	{ OP_PUSH_LIT,   1, 1, 0 },
	{ OP_PUSH_CNL,   1, 1, 0 },
	{ OP_PUSH_TICK,  0, 1, 0 },
	{ OP_PUSH_UNSET, 0, 1, 0 },
	{ OP_ADD,        0, 1, 2 },
	{ OP_SUB,        0, 1, 2 },
	{ OP_MUL,        0, 1, 2 },
	{ OP_DIV,        0, 1, 2 },
	{ OP_MOD,        0, 1, 2 },
	{ OP_IFELSE,     0, 1, 3 },
};

struct func_stack {
	value *stack;
	value *stack_head;
	u64 stack_size;
};

static inline void
func_stack_push(struct func_stack *stack, value val) {
	assert((u64)(stack->stack_head - stack->stack) < stack->stack_size);
	*(stack->stack_head++) = val;
}

static inline value
func_stack_pop(struct func_stack *stack) {
	assert(stack->stack_head > &stack->stack[0]);
	return *(--stack->stack_head);
}

void
func_eval_step(u8 **ip, u8 *instr_end, struct func_stack *stack, value *params, u64 num_params) {
	value pop[3];
	u64 arg[3];

	u8 op = read_u8(ip, instr_end);

	switch (op) {

	case OP_NOP: break;

	case OP_PUSH_LIT:
		arg[0] = read_u64(ip, instr_end);
		func_stack_push(stack, value_from_u64(arg[0]));
		break;

	case OP_PUSH_CNL:
		arg[0] = read_u64(ip, instr_end);
		assert(arg[0] < num_params);
		func_stack_push(stack, params[arg[0]]);
		break;

	case OP_PUSH_TICK: break;

	case OP_PUSH_UNSET:
		func_stack_push(stack, value_from_unset());
		break;

	case OP_ADD:
		pop[0] = func_stack_pop(stack);
		pop[1] = func_stack_pop(stack);
		func_stack_push(stack, value_add(pop[0], pop[1]));
		break;

	case OP_SUB:
		pop[0] = func_stack_pop(stack);
		pop[1] = func_stack_pop(stack);
		func_stack_push(stack, value_subtract(pop[0], pop[1]));
		break;

	case OP_MUL:
		pop[0] = func_stack_pop(stack);
		pop[1] = func_stack_pop(stack);
		func_stack_push(stack, value_multiply(pop[0], pop[1]));
		break;

	case OP_DIV:
		pop[0] = func_stack_pop(stack);
		pop[1] = func_stack_pop(stack);
		func_stack_push(stack, value_divide(pop[0], pop[1]));
		break;

	case OP_MOD:
		pop[0] = func_stack_pop(stack);
		pop[1] = func_stack_pop(stack);
		func_stack_push(stack, value_mod(pop[0], pop[1]));
		break;

	case OP_IFELSE:
		pop[0] = func_stack_pop(stack);
		pop[1] = func_stack_pop(stack);
		pop[2] = func_stack_pop(stack);
		func_stack_push(stack, value_ifelse(pop[0], pop[1], pop[2]));
		break;

	default:
		// Unknown command. The function should already have been
		// validated.
		assert(false);
		break;
	}
}

struct func_eval_op {
	enum func_opcode op;
	bool error;
	bool eof;
	u64 arg[3];
};

struct func_eval_op
func_eval_step_dry(u8 **ip, u8 *instr_end) {
	struct func_eval_op result = {};

	result.op = read_u8(ip, instr_end);

	assert(result.op < sizeof(func_op_data) / sizeof(func_op_data[0]));
	assert(func_op_data[result.op].op == result.op);

	assert(func_op_data[result.op].num_args <= 3);
	for (u64 i = 0; i < func_op_data[result.op].num_args; ++i) {
		result.arg[i] = read_u64(ip, instr_end);
	}
	
	if (*ip >= instr_end) {
		result.eof = true;
	}

	return result;
}

value
func_eval(struct arena *transient, struct func *func, value *params, u64 num_params) {
	u8 *instr_end = func->instr + func->instr_len;
	u8 *ip = func->instr;
	struct func_stack stack = {};

	assert(func->flags & FUNC_VALIDATED);

	stack.stack_size = func->stack_requirement;
	stack.stack = arena_alloc(transient, stack.stack_size, sizeof(value));
	stack.stack_head = stack.stack;

	while (ip < instr_end) {
		func_eval_step(&ip, instr_end, &stack, params, num_params);
	}

	return func_stack_pop(&stack);
}

enum func_validate_error {
	FUNC_VALIDATE_OK = 0,
	FUNC_VALIDATE_UNEXPECTED_EOF = (1<<0),
	FUNC_VALIDATE_POP_PAST_BASE = (1<<1),
	FUNC_VALIDATE_MISSING_ARGUMENT = (1<<2),
	FUNC_VALIDATE_STACK_NOT_ONE_LEFT = (1<<3),
};

u8
func_validate(struct func *func) {
	u8 error = FUNC_VALIDATE_OK;

	u8 *instr_end = func->instr + func->instr_len;
	u8 *ip = func->instr;
	u64 max_stack_usage = 0;
	u64 stack_usage = 0;

	while (ip < instr_end) {
		enum func_opcode op = read_u8(&ip, instr_end);
		u64 args[3];

		assert(func_op_data[op].op == op);

		for (u64 i = 0; i < func_op_data[op].num_args; ++i) {
			// Check that we can read the entire instruction.
			if (ip + sizeof(u64) > instr_end) {
				error |= FUNC_VALIDATE_UNEXPECTED_EOF;
				break;
			}

			args[i] = read_u64(&ip, instr_end);
		}

		// Check that we do not pop too much.
		if (stack_usage < func_op_data[op].num_pop) {
			error |= FUNC_VALIDATE_POP_PAST_BASE;
			break;
		}
		stack_usage -= func_op_data[op].num_pop;

		stack_usage += func_op_data[op].num_push;

		if (stack_usage > max_stack_usage) {
			max_stack_usage = stack_usage;
		}

		if (op == OP_PUSH_CNL) {
			if (args[0] > func->dependencies_len) {
				error |= FUNC_VALIDATE_MISSING_ARGUMENT;
				break;
			}
		}
	}

	if (stack_usage != 1) {
		error |= FUNC_VALIDATE_STACK_NOT_ONE_LEFT;
	}

	if (error) {
		func->stack_requirement = 0;
		func->flags &= ~((u8)FUNC_VALIDATED);
	} else {
		func->stack_requirement = max_stack_usage;
		func->flags |= FUNC_VALIDATED;
	}

	return error;
}
