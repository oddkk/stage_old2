struct fixture_channel {
	struct atom *name;
	struct func *func;
	enum value_type type;
};

struct fixture {
	struct atom *name;
	struct fixture_channel *channels;
	u64 num_channels;
};

#define FIXTURE_REPO_PAGE_CAPACITY (16)

struct fixture_repo_page {
	// SOA
	u64 fixture_names[FIXTURE_REPO_PAGE_CAPACITY];
	struct fixture fixtures[FIXTURE_REPO_PAGE_CAPACITY];
	u64 num;

	struct fixture_repo_page *next;
};

struct fixture_repo {
	// TODO: Maby use heap?
	struct fixture_repo_page *first_page;
};

void
fixture_repo_add_fixture(struct arena *mem, struct fixture_repo *repo, struct fixture fixture) {
	struct fixture_repo_page **page = &repo->first_page;

	while (*page && (*page)->num < FIXTURE_REPO_PAGE_CAPACITY) {
		page = &(*page)->next;
	}

	if (!*page) {
		*page = arena_alloc(mem, 1, sizeof(struct fixture_repo_page));
	}

	assert((*page)->num < FIXTURE_REPO_PAGE_CAPACITY);

	(*page)->fixture_names[(*page)->num] = fixture->name->hash;
	(*page)->fixtures[(*page)->num] = fixture;

	++(*page)->num;
}

struct fixture *
fixture_repo_get_fixture(struct fixture_repo *repo, struct atom *name) {
	// TODO: Use a faster than linear search.
	struct fixture_repo_page *page = repo->first_page;
	u64 hash = name->hash;
	while (page) {
		for (u64 i = 0; i < page->num; ++i) {
			if (page->fixture_names[i] == hash) {
				assert(page->fixture_names[i] == page->fixtures[i]->name->hash);
				return page->fixtures[i];
			}
		}
		page = page->next;
	}
	return 0;
}

struct patch {
	struct fixture_prototype *prototype;
	struct atom *name;
	u64 channel_begin;
};

struct group;

enum group_member_type {
	GROUP_MEMBER_GROUP,
	GROUP_MEMBER_FIXTURE,
};

struct group_member {
	union {
		struct fixture *fixture;
		struct group *group;
	};
	struct group_member *next;
	enum group_member_type type;
};

struct group_channel {
	struct atom *name;
};

struct group {
	struct group_member *first;
	struct group_channel *channels;
	u64 num_channels;
};

void
group_add_group_member(struct group *dest, struct group_member *member) {
	struct group_member **prev = &dest->first;
	while (*prev != 0) {
		prev = &(*prev)->next;
	}
	assert(prev);
	(*prev)->next = member;
}

void
group_add_fixture(struct group *dest, struct fixture *m) {
	struct group_member *member = calloc(1, sizeof(struct group_member));
	member->fixture = m;
	member->type = GROUP_MEMBER_FIXTURE;
	member->next = 0;
	group_add_group_member(dest, member);
}

void
group_add_group(struct group *dest, struct group *m) {
	struct group_member *member = calloc(1, sizeof(struct group_member));
	member->group = m;
	member->type = GROUP_MEMBER_GROUP;
	member->next = 0;
	group_add_group_member(dest, member);
}

struct stage_fixture_bucket {
	struct stage_fixture_bucket *next;
	struct fixture *fixture;
};
