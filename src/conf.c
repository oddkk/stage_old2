enum conf_entry_type {
	CONF_ENTRY_VALUE = 0,
	CONF_ENTRY_BLOCK = 1,
};

enum conf_parser_state {
	CONF_PARSE_STATE_IDLE = 0,
	CONF_PARSE_STATE_NAME,
	CONF_PARSE_STATE_POST_NAME,
	CONF_PARSE_STATE_PRE_VALUE,
	CONF_PARSE_STATE_VALUE,
	CONF_PARSE_STATE_COMMENT,
	CONF_PARSE_STATE_PRE_INCLUDE_FILENAME,
	CONF_PARSE_STATE_INCLUDE_FILENAME,
	CONF_PARSE_STATE_ERROR,
};

#define CONF_ENTRY_NAME_MAX_LENGTH (200)

struct conf_entry {
	enum conf_entry_type type;
	struct string_position pos;
	struct string file;
	struct atom *name;
	struct string value;

	struct conf_entry *parent;
	struct conf_entry *next_sibling;
	struct conf_entry *first_child;
};

struct conf_parse_state {
	enum conf_parser_state state;
	enum conf_parser_state prev_state;

	struct conf_entry *root;
	struct conf_entry *parent;
	struct conf_entry *current;

	struct string_position pos;

	struct arena *mem;
	struct atom_table *atom_table;

	struct string value_buffer;
	u64 value_buffer_capacity;
	struct string_position value_buffer_begin;

	s64 error_block_nesting;

	struct string file;

	struct {
		struct atom *include;
	} atoms;
};

static inline bool
conf_parse_state_push_rune(struct conf_parse_state *state, rune c) {
	u8 buffer[4];
	u64 bytes = utf8proc_encode_char(c, buffer);
	if (bytes == 0) {
		fprintf(stderr, "Could not encode character (%u).\n", c);
		return false;
	}


	if (state->value_buffer.len + bytes >= state->value_buffer_capacity) {
		return false;
	}
	for (u64 i = 0; i < bytes; ++i) {
		state->value_buffer.text[state->value_buffer.len++] = buffer[i]; 
	}
	return true;
}

static inline void
conf_parse_state_add_entry(struct conf_parse_state *state, struct conf_entry *entry) {
	entry->parent = state->parent;
	if (state->current) {
		state->current->next_sibling = entry;
	} else {
		state->parent->first_child = entry;
	}
	state->current = entry;
}

static inline void
conf_parse_state_push_block(struct conf_parse_state *state) {
	state->current->type = CONF_ENTRY_BLOCK;
	state->parent = state->current;
	state->current = 0;
}

static inline bool
conf_parse_state_pop_block(struct conf_parse_state *state) {
	assert(state->parent);
	if (!state->parent->parent) {
		return false;
	}
	state->current = state->parent;
	state->parent = state->parent->parent;
	return true;
}

static inline void
conf_parse_warn_base(struct conf_parse_state *state) {
	fprintf(stderr, "warning: stage conf ");
	fprintf(stderr, "%.*s:%lu (%lu): ",
			LIT(state->file), state->pos.line + 1,
			state->pos.col + 1);
}

static inline void
conf_parse_warn(struct conf_parse_state *state, const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);

	conf_parse_warn_base(state);

	vfprintf(stderr, fmt, ap);
	fprintf(stderr, "\n");

	va_end(ap);
}

static inline void
conf_parse_warn_unexpected_char(struct conf_parse_state *state, rune c) {
	if (c >= ' ' && c <= '~') {
		conf_parse_warn(state, "Got unexpected character '%c'", c);
	} else {
		conf_parse_warn(state, "Got unexpected character (%i)", c);
	}
}

void conf_parse_file_include(struct conf_parse_state *parent_state, struct string path);

static inline void
conf_parse_rune(struct conf_parse_state *state, rune c) {
	if (!state->root) {
	}
	assert(state->parent);

	if (!state->value_buffer.text) {
		state->value_buffer_capacity = CONF_ENTRY_NAME_MAX_LENGTH;
		state->value_buffer.text = arena_alloc(state->mem, state->value_buffer_capacity, sizeof(u32));
		state->value_buffer.len = 0;
	}

	switch (state->state) {

	case CONF_PARSE_STATE_IDLE: {
		if (rune_is_alpha(c)) {
			state->state = CONF_PARSE_STATE_NAME;

			state->value_buffer_begin = state->pos;
			state->value_buffer.len = 0;

			// Reconsume
			conf_parse_rune(state, c);
			return;

		} else if (c == '}') {
			conf_parse_state_pop_block(state);

		} else if (rune_is_whitespace(c)) {
			// Ignore

		} else if (c == '#') {
			state->state = CONF_PARSE_STATE_COMMENT;
			state->prev_state = CONF_PARSE_STATE_IDLE;

		} else if (c == '{') {
			// Unexpected symbol
			conf_parse_warn_unexpected_char(state, c);
			state->state = CONF_PARSE_STATE_ERROR;
			state->error_block_nesting = 1;

		} else {
			// Unexpected symbol
			conf_parse_warn_unexpected_char(state, c);
			state->state = CONF_PARSE_STATE_ERROR;
			state->error_block_nesting = 0;
		}
	} break;

	case CONF_PARSE_STATE_NAME: {
		if (rune_is_alphanum(c) || c == '_' || c == '-') {
			if (!conf_parse_state_push_rune(state, c)) {
				state->state = CONF_PARSE_STATE_ERROR;
			}

		} else {
			struct atom *name = atom_create(state->atom_table, state->value_buffer);

			if (name == state->atoms.include) {
				state->state = CONF_PARSE_STATE_PRE_INCLUDE_FILENAME;

			} else {
				struct conf_entry *entry = arena_alloc(state->mem, 1, sizeof(struct conf_entry));

				entry->name = name;
				entry->parent = state->current;
				entry->pos = state->value_buffer_begin;
				entry->file = state->file;

				conf_parse_state_add_entry(state, entry);

				state->state = CONF_PARSE_STATE_POST_NAME;
			}

			// Reconsume
			conf_parse_rune(state, c);
			return;
		}
	} break;

	case CONF_PARSE_STATE_POST_NAME: {
		if (rune_is_whitespace(c)) {
			// Ignore
		} else if (c == '=') {
			state->state = CONF_PARSE_STATE_PRE_VALUE;

		} else if (c == '{') {
			state->state = CONF_PARSE_STATE_IDLE;
			conf_parse_state_push_block(state);

		} else if (c == '#') {
			state->state = CONF_PARSE_STATE_COMMENT;
			state->prev_state = CONF_PARSE_STATE_POST_NAME;

		} else {
			// Unexpected symbol
			conf_parse_warn_unexpected_char(state, c);
			state->state = CONF_PARSE_STATE_ERROR;
			state->error_block_nesting = 0;
		}
	} break;

	case CONF_PARSE_STATE_PRE_VALUE: {
		if (rune_is_whitespace(c)) {
			// Ignore

		} else if (c == '#') {
			state->state = CONF_PARSE_STATE_COMMENT;
			state->prev_state = CONF_PARSE_STATE_PRE_VALUE;

		} else {
			state->state = CONF_PARSE_STATE_VALUE;

			state->value_buffer_begin = state->pos;
			state->value_buffer.len = 0;

			// Reconsume
			conf_parse_rune(state, c);
			return;
		}
	} break;

	case CONF_PARSE_STATE_VALUE: {
		if (rune_is_alphanum(c) ||
			c == '"' || c == '\'' ||
			c == ' ' || c == '\t' ||
			c == '[' || c == ']' ||
			c == ',' || c == '.') {
			if (!conf_parse_state_push_rune(state, c)) {
				state->state = CONF_PARSE_STATE_ERROR;
			}

		} else if (c == ';') {
			arena_duplicate_string(state->mem, &state->current->value, state->value_buffer);
			state->state = CONF_PARSE_STATE_IDLE;

		} else if (c == '#') {
			state->state = CONF_PARSE_STATE_COMMENT;
			state->prev_state = CONF_PARSE_STATE_VALUE;

		} else if (c == '}') {
			// Unexpected symbol
			conf_parse_warn_unexpected_char(state, c);
			conf_parse_state_pop_block(state);

		} else {
			// Unexpected symbol
			conf_parse_warn_unexpected_char(state, c);
			state->state = CONF_PARSE_STATE_ERROR;
			state->error_block_nesting = 0;
		}
	} break;

	case CONF_PARSE_STATE_COMMENT: {
		if (c == '\n') {
			state->state = state->prev_state;

		} else {
			// Ignore
		}
	} break;

	case CONF_PARSE_STATE_PRE_INCLUDE_FILENAME: {
		if (rune_is_whitespace(c)) {
			// Ignore

		} else if (c == '#') {
			state->state = CONF_PARSE_STATE_COMMENT;
			state->prev_state = CONF_PARSE_STATE_PRE_INCLUDE_FILENAME;

		} else {
			state->state = CONF_PARSE_STATE_INCLUDE_FILENAME;

			state->value_buffer_begin = state->pos;
			state->value_buffer.len = 0;

			// Reconsume
			conf_parse_rune(state, c);
			return;
		}
	} break;

	case CONF_PARSE_STATE_INCLUDE_FILENAME: {
		if (c == ';') {
			state->state = CONF_PARSE_STATE_IDLE;
			conf_parse_file_include(state, state->value_buffer);

		} else if (c == '#') {
			state->state = CONF_PARSE_STATE_COMMENT;
			state->prev_state = CONF_PARSE_STATE_INCLUDE_FILENAME;

		} else {
			conf_parse_state_push_rune(state, c);
		}
	} break;

	case CONF_PARSE_STATE_ERROR: {
		if (c == ';' && state->error_block_nesting == 0) {
			state->state = CONF_PARSE_STATE_IDLE;
		} else if (c == '{') {
			state->error_block_nesting += 1;
		} else if (c == '}') {
			state->error_block_nesting -= 1;
			if (state->error_block_nesting < 0) {
				state->state = CONF_PARSE_STATE_IDLE;
			}
		}
	} break;

	}

	state->pos.col += 1;

	if (c == '\n') {
		state->pos.line += 1;
		state->pos.col = 0;
	}
}

void
conf_parse_file_include(struct conf_parse_state *parent_state, struct string path) {
	struct conf_parse_state state = {
		.mem = parent_state->mem,
		.atom_table = parent_state->atom_table,
		.atoms = parent_state->atoms,
		.root = parent_state->parent,
		.parent = parent_state->parent,
		.current = parent_state->current,
	};

	arena_duplicate_string(state.mem, &state.file, path);

	u64 arena_state = arena_push_state(state.mem);

	int error;

	// TODO: Fix utf-8 compatebility
	char *twd = arena_alloc(state.mem, PATH_MAX, 1);
	if (!getcwd(twd, PATH_MAX)) {
		conf_parse_warn_base(parent_state);
		fprintf(stderr, "include %.*s: ", LIT(path));
		perror(0);
		return;
	}
	u64 twd_len = strnlen(twd, PATH_MAX);

	// Note: Because we need to reset the working directory after
	// parsing the current file, we should not keep it in the
	// arena. This would make us unable to reuse the memory used for
	// the path (PATH_MAX bytes) until the entire parsing process is
	// completed. Because of this we use alloca to make a copy of the
	// current working directory.
	char *wd = alloca(twd_len);
	copy_memory(wd, twd, twd_len);

	char *dir = arena_alloc(state.mem, parent_state->file.len + 1, 1);
	copy_memory(dir, parent_state->file.text, parent_state->file.len);
	dir = dirname(dir);

	error = chdir(dir);
	if (error != 0) {
		conf_parse_warn_base(parent_state);
		fprintf(stderr, "include %.*s: ", LIT(path));
		perror(0);
		return;
	}

	char *filename = arena_alloc(state.mem, path.len + 1, 1);
	copy_memory(filename, path.text, path.len);

	UFILE *ufd = u_fopen(filename, "r", NULL, NULL);
	if (!ufd) {
		conf_parse_warn_base(parent_state);
		fprintf(stderr, "include %.*s: ", LIT(path));
		perror(0);
	}
	
	arena_pop_state(state.mem, arena_state);

	while (!u_feof(ufd)) {
		rune c = u_fgetcx(ufd);
		conf_parse_rune(&state, c);
	}
	parent_state->current = parent_state->parent->first_child;
	while (parent_state->current->next_sibling != 0) {
		parent_state->current = parent_state->current->next_sibling;
	}

	error = chdir(wd);
	if (error != 0) {
		conf_parse_warn_base(parent_state);
		fprintf(stderr, "include %.*s: ", LIT(path));
		perror(0);
		return;
	}

}

struct conf_entry *
conf_parse_file(struct arena *mem, struct atom_table *atom_table, struct string path) {
	struct conf_parse_state state = {
		.mem = mem,
		.atom_table = atom_table,
		.file = path,
	};

	state.atoms.include = atom_create(state.atom_table, STR_FROM_CSTR("include"));

	state.root = arena_alloc(state.mem, 1, sizeof(struct conf_entry));
	state.parent = state.root;

	state.root->type = CONF_ENTRY_BLOCK;
	state.root->name = atom_create(state.atom_table, STR_FROM_CSTR("root"));
	state.root->file = path;

	UFILE *ufd = u_fopen((char *)path.text, "r", NULL, NULL);

	if (!ufd) {
		perror("stage base conf");
	}

	while (!u_feof(ufd)) {
		rune c = u_fgetcx(ufd);
		conf_parse_rune(&state, c);
	}

	return state.root;
}
