#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <time.h>
#include <signal.h>
#include <errno.h>
#include <assert.h>
#include <sched.h>
#include <libgen.h>
#include <unistd.h>
#include <string.h>

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef float f32;
typedef double f64;
typedef enum { false, true } bool;

#include "utf8proc/utf8proc.h"

#include <unicode/ustdio.h>

#include "memory.c"
#include "string.c"
#include "atom.c"
#include "conf.c"

struct program_memory {
	struct arena *transient;
	struct arena *memory;
};

#include "value.c"
#include "func.c"
#include "channel.c"
#include "fixture.c"
#include "conf_loader.c"

static bool should_exit = false;

static inline u64
timespec_to_msec(struct timespec t) {
	return t.tv_nsec * 1e-6 + t.tv_sec * 1e+3;
}

static inline u64
read_time() {
	struct timespec time;
	int error;

	error = clock_gettime(CLOCK_MONOTONIC_RAW, &time);

	// NOTE: We should already have checked that we support
	// CLOCK_MONOTONIC_RAW, so we should not receive EINVAL error. It
	// can return EFAULT if the second param is outside the memory we
	// can access. This would be a programming error.
	assert(!error);

	return timespec_to_msec(time);
}

static inline bool
check_clock_support() {
	struct timespec time;
	int error;
	error = clock_gettime(CLOCK_MONOTONIC_RAW, &time);

	if (error == EINVAL) {
		fprintf(stderr, "CLOCK_MONOTONIC_RAW is not supported on this system.\n");
		return false;
	}

	// NOTE: clock_gettime can return EFAULT if the second param is
	// outside the memory we can access. If that is the case, it is a
	// programming error.
	assert(!error);

	return true;
}

void
signal_handler(int sig) {
	if (sig == SIGINT) {
		should_exit = true;
	}
}

void
print_value(FILE *fp, value v) {
	fprintf(fp, "{");
	switch (v.type) {
	case VALUE_UNSET: fprintf(fp, "unset"); break;
	case VALUE_U64: fprintf(fp, "u64"); break;
	/* case VALUE_U32: fprintf(fp, "u32"); break; */
	/* case VALUE_U16: fprintf(fp, "u16"); break; */
	/* case VALUE_U8:  fprintf(fp, "u8"); break; */
	/* case VALUE_BOOL:  fprintf(fp, "bool"); break; */
	/* case VALUE_NUMBER:  fprintf(fp, "number"); break; */
	/* case VALUE_NORMAL:  fprintf(fp, "normal"); break; */
	}
	//if (value_is_int(v)) {
	if (value_is_set(v)) {
		fprintf(fp, ": %u}", v.v_u64);
	/* } else if (value_is_float(v)) { */
	/* 	fprintf(fp, ": %f}", v.v_number); */
	} else {
		fprintf(fp, "}");
	}
}

void
print_conf_entry(struct conf_entry *entry, struct string prefix, u64 prefix_len) {
	printf("(%.*s:%lu, %lu) ", LIT(entry->file), entry->pos.line + 1, entry->pos.col + 1);
	if (!entry->name) {
		printf("no name");
	} else {	
		switch (entry->type) {
		case CONF_ENTRY_BLOCK:
			printf("Block %.*s", LIT(entry->name->name));
			break;
		case CONF_ENTRY_VALUE: 
			printf("Value %.*s: %.*s", LIT(entry->name->name), LIT(entry->value));
			break;
		}
	}

	printf("\n");

	prefix.text[prefix_len] = ' ';
	prefix.text[prefix_len+1] = '|';

	for (struct conf_entry *child = entry->first_child;  child != 0; child = child->next_sibling) {
		if (!child->next_sibling) {
			prefix.text[prefix_len+1] = ' ';
		}
		printf("%.*s |-", (int)(prefix_len < prefix.len ? prefix_len : prefix.len), prefix.text);
		print_conf_entry(child, prefix, prefix_len+2);
	}
}

void
print_conf_tree(struct conf_entry *root) {
	struct string prefix_buffer = {};
	prefix_buffer.len = 20;
	prefix_buffer.text = calloc(prefix_buffer.len, sizeof(u8));

	print_conf_entry(root, prefix_buffer, 0);

	free(prefix_buffer.text);
}

static void conf_load_warn_base(struct conf_entry *entry) {
	fprintf(stderr, "warning: stage conf ");
	fprintf(stderr, "%.*s:%lu (%lu): ",
			LIT(entry->file), entry->pos.line + 1,
			entry->pos.col + 1);
}

void
load_fixtures(struct arena *transient, struct atom_table *atom_table, struct fixture_repo *repo, struct conf_entry *scope) {
	struct {
		struct atom *fixture;
		struct atom *name;
	} atoms;

	atoms.fixture = atom_create(atom_table, STR_FROM_CSTR("fixture"));
	atoms.name = atom_create(atom_table, STR_FROM_CSTR("name"));

	struct conf_entry *conf_fixture, *it = scope->first_child;
	while ((conf_fixture = conf_find_next_sibling(&it, atoms.fixture))) {
		struct conf_entry *conf_name = conf_find_child(conf_fixture, atoms.name);
		if (!conf_name) {
			conf_load_warn_base(conf_fixture);
			fprintf(stderr, "Fixture missing required parameter \"name\"\n");
			continue;
		}
		struct atom *name = conf_entry_as_atom(conf_name, atom_table);
		printf("fixture: %.*s\n", LIT(name->name));
	}
}


// If this is a test, do not include the main function. It will be
// defined elsewhere.
#ifndef STAGE_TEST
int
main() {
	struct arena memory = {};
	memory.capacity = MEBIBYTES(10);
	memory.data = calloc(memory.capacity, 1);

	struct arena transient = {};
	transient.capacity = MEBIBYTES(1);
	transient.data = arena_alloc(&memory, transient.capacity, 1);

	/* struct program_memory mem = { */
	/* 	.memory = &memory, */
	/* 	.transient = &transient, */
	/* }; */

	struct atom_table atom_table = {};
	atom_table.string_arena = &memory;

	atom_table_rehash(&atom_table, 20);

	struct conf_entry *conf = conf_parse_file(&transient, &atom_table, STR_FROM_CSTR("./conf/stage.conf"));

	struct fixture_repo fixture_repo = {};

	load_fixtures(&transient, &atom_table, &fixture_repo, conf);

	/*
	u64 channels  = conf_entry_as_u64(conf_find_child(conf, atom_create(&atom_table, STR_FROM_CSTR("channels"))));
	printf("channels: %lu\n", channels);
	*/

	//print_conf_tree(conf);


	free(memory.data);

	return 0;
}
#endif
