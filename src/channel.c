struct channel {
	value value;
	struct atom *name;
	struct func *func;
};

struct channel_range {
	struct channel *channels;
	u64 channel_begin;
	u64 channel_end;
};

static inline struct channel *
channel_get(struct channel_range *channels, u64 channel) {
	if (channel < channels->channel_begin ||
		channel > channels->channel_end) {
		return 0;
	}
	return &channels->channels[channel - channels->channel_begin];
}

static inline struct channel *
channel_get_by_name(struct channel_range *channels, struct atom *name) {
	// TODO: Use an algorithm with better than O(n) complexity
	for (u64 i = channels->channel_begin;
		 i < channels->channel_end; ++i) {
		if (channels->channels[i - channels->channel_begin].name == name) {
			return &channels->channels[i - channels->channel_begin];
		}
	}
	return 0;
}

value
channel_get_value(struct arena *transient, struct channel_range *channels, u64 channel) {
	struct channel *cnl = channel_get(channels, channel);

	if (value_is_set(cnl->value)) {
		return cnl->value;
	}

	if (cnl->func) {
		u64 num_args = cnl->func->dependencies_len;
		value *args = arena_alloc(transient, num_args, sizeof(u64));

		for (u64 i = 0; i < num_args; ++i) {
			if (cnl->func->dependencies[i] == channel) {
				fprintf(stderr, "warning: Circular dependency.\n");
				continue;
			}
			args[i] = channel_get_value(transient, channels,
										cnl->func->dependencies[i]);
		}

		return func_eval(transient, cnl->func, args, num_args);
	}

	return value_from_unset();
}
