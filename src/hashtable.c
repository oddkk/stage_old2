struct hashtable_bucket {
	struct hash_table_bucket *next;
	u64 hash;
	void *data;
};

void *
hashtable_find_hash(struct hashtable_bucket *buckets, u64 num_buckets, u64 hash) {
	assert(buckets);
	assert(num_buckets > 0);

	struct hashtable_bucket **current;

	current = &buckets[hash % num_buckets];

	while (*current && (*current)->hash <= hash) {
		if ((*current)->hash == hash) {
			return *current->data;
		}
		current = &(*current)->next;
	}

	return 0;
}

void *
hashtable_insert(struct hashtable_bucket *buckets, u64 num_buckets, struct hashtable_bucket *bucket) {
	assert(buckets);
	assert(num_buckets > 0);

	struct hashtable_bucket **current;

	current = &buckets[hash % num_buckets];

	while (*current && (*current)->hash <= hash) {
		if ((*current)->hash == hash) {
			break;
		}
		current = &(*current)->next;
	}

	return 0;
}
