#!/bin/bash

CC=clang
VERSION=0.1

$CC -Wall -Wextra -pedantic -Wno-gnu-empty-initializer -Wno-unused-function -DVERSION=\"${VERSION}\" -D_DEFAULT_SOURCE -D_POSIX_C_SOURCE=200809L -g -std=c99 -lrt -lpthread -o test -Isrc/ -Itests/ tests/test.c

./test
